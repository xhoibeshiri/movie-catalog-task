import { Component, OnInit } from '@angular/core';
import {ApiService} from "../service/api.service";
import {FormBuilder, Validators} from "@angular/forms";
import {Movie} from "./const";

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {
  movies : Movie[] = [];
  filterForm !: any;
  star = '';

  constructor(private service: ApiService, private fb: FormBuilder) { }

  ngOnInit(): void {
    this.filterForm = this.fb.group({
      movie: ['', Validators.required]
    });
  }

  getMovies(){
    this.service.getMovies(this.filterForm.value.movie).subscribe({
      next: (res)=> {
        if(res['Response']=='True') this.movies = res['Search']?.slice(0,3);
        else alert(res['Error'])
      }, error: (error) => {
        console.log(error)
      }, complete: ()=>{
        this.filterForm.reset();
        for (let i = 0; i < 3; i++) {
          this.movies[i]['rating'] = String(0);
        }
      }
    });
  }

  rate(value: string, id:string) {
    for (let i=0; i<3; i++){
      if(this.movies[i].imdbID==id){
        this.movies[i].rating = value;
        break;
      }
    }
  }
}
