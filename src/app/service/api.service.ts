import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private apiKey = 'dd888f29'

  constructor(private http: HttpClient) {}

  getMovies (title:string): Observable<any>{
    return this.http.get(`https://www.omdbapi.com/?apikey=${this.apiKey}&s=${title}`);
  }
}
